package CommonTools;

public class StringLiterals {

    // TODO: these user credential should not be hardcoded
    public static final String JDBC_MYSQL_192_168_2_3_3306_RTADATEN = "jdbc:mysql://192.168.2.3:3306/rtadaten";
    public static final String RTAUSER = "rtauser";
    public static final String RTACURIE = "rtacurie";
    public static final String RTA_IK = "510841109";

    public static final String C_PROGRAMME_OPEN_OFFICE_ORG_3 = "C:/Programme/OpenOffice.org 3";
    public static final String C_REHA_VERWALTUNG_LIBRARIES_LIB_OPENOFFICEORG = "C:/RehaVerwaltung/Libraries/lib/openofficeorg/";
    public static final String C_REHA_VERWALTUNG = "C:/RehaVerwaltung/";
    public static final String DATEN_BANK_SECTION = "DatenBank";
}
